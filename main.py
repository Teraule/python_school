import argparse
import datetime as dt

from models.fabric import ModelFabric
from services import Dumper
from services import PersonGrouper


def parse_args():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        description='Person processor'
    )

    parser.add_argument('--start_date', type=dt.date.fromisoformat,
                        default=dt.datetime.now().date() - dt.timedelta(days=30), help='Date of interval start')

    parser.add_argument('--end_date', type=dt.date.fromisoformat,
                        default=dt.datetime.now().date(), help='Date of interval end')

    return parser.parse_args()


if __name__ == '__main__':
    args = parse_args()

    fabric = ModelFabric(args.start_date, args.end_date)
    persons = list(fabric.create_persons(10))
    print(Dumper.dumps([p.as_dict() for p in persons]))

    grouped_by_sex = PersonGrouper.group_by_sex(persons)
    grouped_by_date = PersonGrouper.group_by_birth_date(persons)

    print(grouped_by_sex)
    print(grouped_by_date)
