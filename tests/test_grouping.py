import json
import pytest

from hamcrest import assert_that, all_of, contains_inanyorder

from models.person import Person, Sex
from services import PersonGrouper


@pytest.fixture
def persons():
    fp = open('persons.json', 'r')
    persons = [Person.from_dict(p) for p in json.load(fp)]
    yield persons
    print('teardown "persons"')
    fp.close()


@pytest.fixture
def persons_grouped_by_sex():
    fp = open('persons_by_sex.json', 'r')
    persons_by_sex_dict = json.load(fp)
    persons_by_sex = {Sex(sex): [Person.from_dict(p) for p in gp] for sex, gp in persons_by_sex_dict.items()}
    yield persons_by_sex
    print('teardown "persons_grouped_by_sex"')
    fp.close()


def test_grouping_by_sex(persons, persons_grouped_by_sex):
    grouped_by_sex_result = PersonGrouper.group_by_sex(persons)
    assert not set(grouped_by_sex_result.keys()).symmetric_difference(persons_grouped_by_sex.keys()), "группы разные"
    for gr, pers in persons_grouped_by_sex.items():
        assert_that(pers, contains_inanyorder(*grouped_by_sex_result[gr]))
