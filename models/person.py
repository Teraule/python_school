from datetime import date
from enum import Enum


class Sex(Enum):
    MALE = 'male'
    FEMALE = 'female'


class Person:
    def __init__(self, name: str = None, birth_date: date = None, sex: Sex = Sex.MALE):
        self._name = name
        self._birth_date = birth_date
        self._sex = sex

    @staticmethod
    def from_dict(person_dict):
        person = Person(name=person_dict['name'], birth_date=date.fromisoformat(person_dict['birth_date']),
                        sex=Sex(person_dict['sex']))
        return person

    def as_dict(self):
        return {
            'name': self._name,
            'birth_date': self._birth_date.isoformat(),
            'sex': self._sex.value
        }

    @property
    def name(self):
        return self._name

    @property
    def sex(self):
        return self._sex

    @property
    def birth_date(self):
        return self._birth_date

    def __repr__(self):
        return f'{self._name} {self._birth_date} {self._sex}'

    # for equals checking
    def __eq__(self, other):
        return self._name == other.name and self._sex == other.sex and self._birth_date == other.birth_date
