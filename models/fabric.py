import random as rnd
from datetime import datetime, timedelta

from models.person import Person, Sex


class ModelFabric:

    NAMES = ('Jonh', 'Bill', 'Mark', ' Ivan', 'Rob', 'Mike', 'Bard')
    SURNAMES = ('Rod', 'Simpson', 'Linkoln', 'Brown', 'Smith', 'Gonzales', 'Kurampcha')

    def __init__(self, start_date, end_date):
        self._start_date = start_date
        self._end_date = end_date
        self._date_interval = self._end_date - self._start_date

    def create_persons(self, num):
        for i in range(num):
            yield self.create_person()

    def create_person(self):
        return Person(
            name=f'{rnd.choice(ModelFabric.NAMES)} {rnd.choice(ModelFabric.SURNAMES)}',
            birth_date=self._random_datetime().date(),
            sex=rnd.choice([s for s in Sex])
        )

    def _random_datetime(self):
        return datetime.combine(self._start_date, datetime.min.time()) +\
               timedelta(seconds=rnd.randint(0, int(self._date_interval.total_seconds())))
