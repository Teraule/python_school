from itertools import groupby


class PersonGrouper:
    @staticmethod
    def group_by_sex(persons):
        return {k: list(v) for k, v in groupby(sorted(persons, key=lambda p: p.sex.value), lambda p: p.sex)}

    @staticmethod
    def group_by_birth_date(persons):
        return {k: list(v) for k, v in groupby(sorted(persons, key=lambda p: p.birth_date.year),
                                               lambda p: p.birth_date.year)}
