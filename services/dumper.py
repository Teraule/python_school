import json


class Dumper:
    def __init__(self, file_name):
        self._fname = file_name

    def dump(self, data):
        with open(self._fname, 'w') as f:
            json.dump(f, data)

    @staticmethod
    def dumps(data):
        return json.dumps(data, indent=4)
